<?php

#region Process configuration
// cargar la matriz de configuracion
$matriz = parse_ini_file("smtp_config.ini", true);

$serverConfig = isset($_REQUEST['server']) ? $matriz[$_REQUEST['server']] : end($matriz);
#endregion

#region Capture $_POST vars and validations
$defaultMail = 'default@mail.com';
$defaultValue = 'default';

$from = isset($_REQUEST['from']) ? $_REQUEST['from'] : '';
$fromName = isset($_REQUEST['from_name']) ? $_REQUEST['from_name'] : '';
$replyTo = isset($_REQUEST['reply_to']) ? $_REQUEST['reply_to'] : '';
$to = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
$toName = isset($_REQUEST['realname']) ? $_REQUEST['realname'] : '';
$bcc = isset($_REQUEST['bcc']) ? $_REQUEST['bcc'] : '';
$subject = isset($_REQUEST['subject']) ? $_REQUEST['subject'] : '';
$msg = isset($_REQUEST['msg']) ? $_REQUEST['msg'] : '';

$debug = isset($_REQUEST['debug']) ? $_REQUEST['debug'] : false;

$errors = array();
if (!is_email($from)) {
	$errors['from'] = 'From value is not a valid email address.';
}

if (!empty($replyTo) && !is_email($replyTo)) {
	$errors['reply_to'] = 'Reply to value is not a valid email address.';
}

if (!is_email($to)) {
	$errors['to'] = 'To value is not a valid email address.';
}

if (count($errors) > 0) {
	$data = array(
		'success' => false,
		'errors' => $errors,
	);
	print_r(json_encode($data));
	exit;
}

function is_email($value='')
{
	if (empty($value)) {
		return false;
	}
	return true;
}
#endregion

require_once 'vendor/autoload.php';
require_once dirname(__FILE__) . '/le_php-master/logentries.php';

// Gmail example
//Create a new PHPMailer instance
$mail = new PHPMailer;

//Tell PHPMailer to use SMTP
$mail->isSMTP();

if ($debug) {
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	$mail->SMTPDebug = 3;

	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';
}

//Set the hostname of the mail server
$mail->Host = $serverConfig['host'];
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6

//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = $serverConfig['port'];

//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = $serverConfig['encryption'];

//Whether to use SMTP authentication
$mail->SMTPAuth = $serverConfig['auth'] == 1 ? true : false;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = $serverConfig['user'];

//Password to use for SMTP authentication
$mail->Password = $serverConfig['pass'];

//Set who the message is to be sent from
$mail->setFrom($from, $fromName);

//Set an alternative reply-to address
if(!empty($replyTo))
	$mail->addReplyTo($replyTo);

//Set who the message is to be sent to
$mail->addAddress($to, $toName);

if (!empty($bcc)) {
	$mail->addBCC($bcc);
}

//Set the subject line
$mail->Subject = $subject;

$mail->isHTML(false);

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
//$mail->msgHTML($msg);

$mail->Body = $msg;

//Replace the plain text body with one created manually
//$mail->AltBody = 'This is a plain-text message body';

//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
	$data['success'] = false;
	$data['msg'] = "Mailer Error: " . $mail->ErrorInfo;

	//set the log
	$log->Error($data['msg']);
} else {
	$data['success'] = true;
	$data['msg'] = "Message sent!";

	// set log with info msg
	$log->Debug("Mensaje enviado a: " . $to);
	$log->Info($msg);
}
exit;