<?php
$isPost = true;
$data['url'] = 'http://localhost/mail-service/';
$data['posted'] = array(
	'server' => 'localhost',
	'from' => 'info@fiestadoble.com',
	'reply_to' => 'no-reply@fiestadoble.com',
	'realname' => 'Pablito',
	'to' => 'pmolina@nauta.cu',
	'subject' => 'Asunto del mensaje',
	'msg' => 'Este es el mensaje',
);

$ch  = curl_init();
if ($isPost === false) {
    curl_setopt($ch, CURLOPT_POST, false);
} else {
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data['posted']);
}
curl_setopt($ch, CURLOPT_URL, $data['url']);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 0);

//curl_setopt($ch, CURLOPT_PROXY, '200.200.93.5');
//curl_setopt($ch, CURLOPT_PROXYPORT, '8080');

$r = curl_exec($ch);
if (curl_errno($ch) != CURLE_OK) {
    //$ret = array('status' => 'error', 'data' => curl_error($ch));
} else {
    //$ret = array('status' => 'success', 'data' => trim($r));
}
curl_close($ch);

echo "<pre>";
print_r($r);
exit;