Cuando se instale la aplicación, esta debe contener un fichero llamado `smtp_config.ini`
en la cual se definan bloques de configuración con los diferentes servicios para el 
envío de correos. 

Un ejemplo de ello puede ser:
```
[gmail]
user=myaccount@gmail.com
pass=mypassword
host=smtp.gmail.com
port=465
auth=1
encryption=ssl
```